package tony.nz.cucumbertest

import android.util.Log
import org.junit.Assert
import org.junit.Test

import org.junit.Assert.*

class ConverterTest {
    private val TAG = "ConverterTest"
    val inputs = listOf(0, 1, 2, 3, 4, 5, 6, 7, 8, 9)

    val outputs = listOf<String>("zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine")
    @Test
    fun english_number() {

        inputs.forEachIndexed { index, s ->

            val output = Converter.english_number(s)
            Assert.assertEquals(outputs[index], output)
        }

    }

    @Test
    fun convertNumber() {

        val s = "1235.112"
        val output = Converter.convert(s)

        Log.d(TAG, output)
    }

    @Test
    fun capture() {
        val input = "John Smith\n\"123.45\""

        Converter.capture(input).apply {
            val name = this.first
            val number = this.second

            assertEquals("John Smith", name)
            assertEquals("123.45", number)

        }

        val noName = "\n\"123.45\""
        Converter.capture(noName).apply {
            val name = this.first
            val number = this.second

            assertEquals("\"123.45\"", name)
            assertEquals(null, number)
        }

        val noNumber0 = "John Smith"
        Converter.capture(noNumber0).apply {
            val name = this.first
            val number = this.second

            assertEquals("John Smith", name)
            assertEquals(null, number)
        }

        val noNumber1 = "John Smith\n"
        Converter.capture(noNumber0).apply {
            val name = this.first
            val number = this.second

            assertEquals("John Smith", name)
            assertEquals(null, number)
        }

        val emptyString = ""
        Converter.capture(emptyString).apply {
            val name = this.first
            val number = this.second

            assertEquals(null, name)
            assertEquals(null, number)
        }
    }
}