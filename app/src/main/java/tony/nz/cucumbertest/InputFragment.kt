package tony.nz.cucumbertest


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.findNavController
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_input.*
import java.lang.Exception

/**
 * A simple [Fragment] subclass.
 *
 */
class InputFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_input, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        view.findViewById<Button>(R.id.showOutputBtn).setOnClickListener{
            this.onShowOutputClicked(it)
        }
    }

    fun onShowOutputClicked(v: View){

        Observable.create<Bundle> {
            emitter ->

            val nameAndNumber = Converter.capture(inputEt.text.toString())

            val numberWords = try{
                Converter.convert(nameAndNumber.second?:"")
            }catch (e: Exception){
                e.localizedMessage
            }
            var args = Bundle().apply {
                this.putString("name", nameAndNumber.first)
                this.putString("numberWords", numberWords)
            }

            //pretend to be busy :-)
            Thread.sleep(1000)
            emitter.onNext(args)
            emitter.onComplete()
        }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe{
                (activity as MainActivity).let { it.showProgressDialog() }
            }
            .doOnComplete{
                (activity as MainActivity).let { it.hideProgressDialog() }
            }
            .subscribe {args->
                (activity as MainActivity).toOutputScreen(args)
        }

    }

}
