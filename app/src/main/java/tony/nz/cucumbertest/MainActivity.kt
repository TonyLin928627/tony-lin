package tony.nz.cucumbertest

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.fragment.app.FragmentActivity
import androidx.navigation.NavController
import androidx.navigation.findNavController

class MainActivity : FragmentActivity() {

    private var mNavController : NavController? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mNavController = findNavController(R.id.nav_host_fragment)
    }

    override  fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                mNavController?.let {navController->
                    when(navController.currentDestination?.id){
                        R.id.outputFragment->{
                            navController.popBackStack()
                        }

                        else->{}
                    }
                }

                return true
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {

        mNavController?.let {navController->
            when(navController.currentDestination?.id){
                R.id.outputFragment->{
                    navController.popBackStack()
                }

                else->{
                    super.onBackPressed()
                }
            }
        }

    }

    private var mProgressDialog: ProgressDialog? = null
    fun showProgressDialog() {

        mProgressDialog?.dismiss()

        ProgressDialog(this).let {
            this.mProgressDialog = it
            it.setMessage("In Progress...")
            it.setCanceledOnTouchOutside(false)
            it.show()
        }

    }

    fun hideProgressDialog() {
        mProgressDialog?.dismiss()
        mProgressDialog = null
    }

    fun toOutputScreen(args: Bundle?) {
        mNavController?.navigate(R.id.action_inputFragment_to_outputFragment, args)
    }

}
