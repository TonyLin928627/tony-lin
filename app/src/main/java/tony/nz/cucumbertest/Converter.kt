package tony.nz.cucumbertest

import java.lang.NumberFormatException

typealias NameAndNumber = Pair<String?, String?>

object Converter{

    private val to_19 = arrayOf(
        "zero",
        "one",
        "two",
        "three",
        "four",
        "five",
        "six",
        "seven",
        "eight",
        "nine",
        "ten",
        "eleven",
        "twelve",
        "thirteen",
        "fourteen",
        "fifteen",
        "sixteen",
        "seventeen",
        "eighteen",
        "nineteen"
    )
    private val tens = arrayOf("twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety")
    private val denom = arrayOf(
        "",
        "thousand",
        "million",
        "billion",
        "trillion",
        "quadrillion",
        "quintillion",
        "sextillion",
        "septillion",
        "octillion",
        "nonillion",
        "decillion",
        "undecillion",
        "duodecillion",
        "tredecillion",
        "quattuordecillion",
        "sexdecillion",
        "septendecillion",
        "octodecillion",
        "novemdecillion",
        "vigintillion"
    )


    // convert a value < 100 to English.
    private fun convert_nn(value: Int): String {

        if (value < 20)
            return to_19[value]
        for (v in 0 until tens.size) {
            val dcap = tens[v]
            val dval = 20 + 10 * v
            if (dval + 10 > value) {
                return if (value % 10 != 0) dcap + "-" + to_19[value % 10] else dcap
            }
        }
        throw Exception("Should never get here, less than 100 failure")
    }

    // convert a value < 1000 to english, special cased because it is the level that kicks
    // off the < 100 special case.  The rest are more general.  This also allows you to
    // get strings in the form of "forty-five hundred" if called directly.
    private fun convert_nnn(value: Int): String {
        var word = ""
        val rem = value / 100
        val mod = value % 100
        if (rem > 0) {
            word = to_19[rem] + " hundred"
            if (mod > 0) {
                word = "$word "
            }
        }
        if (mod > 0) {
            word = word + convert_nn(mod)
        }
        return word
    }

    fun english_number(value: Int): String {

        return when{
            value<100 -> convert_nn(value)
            value<1000 -> convert_nnn(value)
            else->{
                var rtn = ""
                for (v in 0 until denom.size) {
                    val didx = v - 1
                    val dval = Math.pow(1000.0, v.toDouble()).toInt()
                    if (dval > value) {
                        val mod = Math.pow(1000.0, didx.toDouble()).toInt()
                        val l = value / mod
                        val r = value - l * mod
                        var ret = convert_nnn(l) + " " + denom[didx]
                        if (r > 0) {
                            ret = ret + ", " + english_number(r)
                        }
                        rtn = ret
                        break
                    }
                }
                rtn
            }
        }
        if (value < 100) {
            return convert_nn(value)
        }
        if (value < 1000) {
            return convert_nnn(value)
        }

        for (v in 0 until denom.size) {
            val didx = v - 1
            val dval = Math.pow(1000.0, v.toDouble()).toInt()
            if (dval > value) {
                val mod = Math.pow(1000.0, didx.toDouble()).toInt()
                val l = value / mod
                val r = value - l * mod
                var ret = convert_nnn(l) + " " + denom[didx]
                if (r > 0) {
                    ret = ret + ", " + english_number(r)
                }
                return ret
            }
        }
        throw Exception("Should never get here, bottomed out in english_number")
    }

    fun convert(numberStr: String): String{


            return numberStr.toFloatOrNull()?.let {
                var number = it
                val prefix = if (it<0){
                    number = -number
                    "Negative"
                }else{
                    ""
                }

                var integer = number.toInt()
                var decimal = (number-integer).toString().substring(2).let {
                    if (it.length>2){
                        it.substring(0, 2)
                    }else{
                        it
                    }
                } .toInt()

                val integerStr = when{
                    integer == 0 ->{""}
                    integer==1 ->{ "${english_number(integer)} dollar" }
                    else ->{ "${english_number(integer)} dollars" }
                }
                val decimalStr = when{
                    decimal == 0 ->{""}
                    decimal==1 ->{ "${english_number(decimal)} cent" }
                    else ->{ "${english_number(decimal)} cents" }
                }

                if(integerStr.isBlank() && decimalStr.isBlank()){
//                    "Free"
                    "Zero dollars and zero cents"
                }
                else if(integerStr.isBlank()){
                    "$prefix $decimalStr".trim()
                }
                else if (decimalStr.isBlank()){
                    "$prefix $integerStr".trim()
                }
                else{
                    "$prefix $integerStr and $decimalStr".trim()
                }
            }?: kotlin.run {
                "Invalid number $numberStr"
            }




    }


    fun capture(input: String): NameAndNumber{
        val output = input.split('\n').filter { it.isNotBlank() }

        var rtn =  when(output.size){
            0-> Pair(null, null)
            1->Pair(output[0], null)
            else->Pair(output[0], output[1])
        }


        rtn.second?.toCharArray()?.filter { numberCharSet.contains(it) }?.let {
            val sb = StringBuffer()

            for (c in it){
                sb.append(c)
            }

            rtn = Pair(rtn.first, sb.toString())
        }

        return rtn
    }

    private val numberCharSet = setOf<Char>(
        '0', '1','2', '3','4', '5','6', '7','8', '9','.'
    )
}