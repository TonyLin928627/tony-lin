package tony.nz.cucumbertest


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import kotlinx.android.synthetic.main.fragment_output.*
import android.content.Intent
import android.view.MenuItem


class OutputFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_output, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let{args->
            args.getString("name")?.let {name->
                nameTv.text = name
            } ?: kotlin.run {
                nameTv.text = "No Name"
            }

            args.getString("numberWords")?.let {numberWords->
                numberWordsTv.text = numberWords
            } ?: kotlin.run {
                numberWordsTv.text = "Invalid number"
            }

            ( activity as FragmentActivity).let {
                it.setActionBar(toolbar)
                it.actionBar.setDisplayHomeAsUpEnabled(true)
                it.actionBar.setDisplayShowHomeEnabled(true)
            }


        }
    }
}
